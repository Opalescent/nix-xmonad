import XMonad
import XMonad.Config (def)

myTerminal = "kitty --single-instance"

main = do
  xmonad $ def { terminal = myTerminal }
