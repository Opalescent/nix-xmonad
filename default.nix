{ stdenv, mkDerivation, base, xmonad }:

mkDerivation {
  pname = "my-xmonad";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [ base xmonad ];
  license = stdenv.lib.licenses.bsd3;
}
